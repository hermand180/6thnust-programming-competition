![NUST Logo](../images/nust.png)

Challenge for High School Teams
============================

<p style='text-align: justify; font-size: 1.2em;'><b>Problem A: Decade or Lustrum</b><br><br/>In ancient times, measurement of time was mostly done in <b>Decade</b>(plural <b>decades</b>) a period of 10 years and or A <b>lustrum</b> (plural <b>lustra</b>) a term for a five-year period. Create a program that takes in the following from user input. A person’s full names and their age in total number of days. The program should then calculate the person’s age in years, decade or lustrum and in which year the person was born. <i>[Hint: Assume there are a total of 365 days in a year ]</i> </p>

<p style='text-align: justify; font-size: 1.2em;'><b>Sample Input 1 :</b>  <br/>
Enter full name: <i> John Doe </i><br/>
Enter age in total number of days: <i> 1058</i><br/><b>Sample Output 1 :</b><br/>Good day John Doe, you are 29 years old. <br/>That means you have lived for over 2 decades and you were born in 1990 
</p>   

<p style='text-align: justify; font-size: 1.2em;'><b>Sample Input 2:</b>
  <br>
  Enter full name: <i> Nangombe Kelly</i>
  <br/>Enter age in total number of days: <i> 219</i>
  <br/><b>Sample Output 2:</b>
  <br/>Good day Nangombe Kelly, you are 6 years old. 
  <br/>That means you have lived for over 1 lustrum and you were born in 2015</p>









